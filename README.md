# @rdub/next-plotly
[Plotly] utils for [Next.js]

<a href="https://npmjs.org/package/@rdub/next-plotly" title="View @rdub/next-plotly on NPM"><img src="https://img.shields.io/npm/v/@rdub/next-plotly.svg" alt="@rdub/next-plotly NPM version" /></a>

- [plot-wrapper.tsx](src/plot-wrapper.tsx): wrap a react-plotly.js Plot, add support for: 
  - A "fallback" image (rendered while `react-plotly.js` is being dynamically loaded)
  - Legend-trace mouseover events
  - Rounding x-axis range in response to user zoom events (e.g. to avoid partial visibility of bars at x-axis ticks)
  - Downloading a plot image (e.g. as PNG)
- [plot.tsx](src/plot.tsx) - wraps `PlotWrapper`, adds default title/subtitle/filtering configs. 

Used in:
- [ctbk.dev]
- [crashes.hudcostreets.org]

[Plotly]: https://plotly.com/
[Next.js]: https://nextjs.org/

[ctbk.dev]: https://ctbk.dev/
[crashes.hudcostreets.org]: https://crashes.hudcostreets.org/
