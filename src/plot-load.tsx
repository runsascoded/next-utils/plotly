import { fromEntries } from "@rdub/base/objs"
import { loadJsonSync } from "@rdub/base/json/load"
import { PlotParams } from "react-plotly.js"
import { CSSProperties } from "react"
import { PlotsDict, PlotSpec } from "./plot"

export function loadPlot<PP extends PlotParams = PlotParams>(spec: PlotSpec, dir: string = "public/plots"): PP {
    const { id, name, style } = spec
    const plot = loadJsonSync<PlotParams>(`${dir}/${name || id}.json`)
    if (style) {
        plot.style = style as (CSSProperties | undefined)
    }
    return plot as PP
}

export function loadPlots(specs: PlotSpec[], dir: string = "public/plots"): PlotsDict {
    return fromEntries(
        specs.map(spec => {
            const plot = loadPlot(spec)
            return [ spec.id, plot ]
        })
    )
}
