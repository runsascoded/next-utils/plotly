import css from "./plot.module.scss";
import ImageModule from "next/image";
import React, { Dispatch, useEffect, useRef, useState } from "react";
import { Figure, PlotParams } from "react-plotly.js";
import dynamic from "next/dynamic";
import getBasePath from "@rdub/next-base/basePath";
import { DownloadImgopts, PlotRelayoutEvent } from "plotly.js";
import { entries, mapValues } from "@rdub/base/objs";
// TODO: debug why "next/image" gets loaded on client as an object containing a "default" member.
const Image = (ImageModule as any)['default'] || ImageModule

const Plotly = dynamic(() => import("react-plotly.js").then(m => {
  // TODO: debug why "react-plotly.js" gets loaded on client as an object containing a "default" member.
  return m['default'] || m
}), { ssr: false })

export type LegendHandlers<TraceName extends string = string> = {
  onLegendClick?: (name: TraceName) => boolean
  onLegendDoubleClick?: (name: TraceName) => boolean
  onLegendMouseOver?: (name: TraceName) => boolean
  onLegendMouseOut?: (name: TraceName) => boolean
}

export type OtherHandlers = {
  onXRange?: (min: number, max: number) => [ number, number ] | void
  onRelayout?: (e: PlotRelayoutEvent, div: HTMLDivElement) => void
}

export type DownloadFmt = DownloadImgopts['format'] | 'jpg'

export type Download = true | DownloadFmt | {
  format?: DownloadFmt
  width?: number
  height?: number
  filename?: string
}

export type Props<Trace extends string = string> = {
  params: PlotParams
  src: string
  alt: string
  height?: number | string
  setXRange?: Dispatch<[number, number] | null>
  basePath?: string
  className?: string
  download?: Download
} & LegendHandlers<Trace> & OtherHandlers

type Initialized = {
  figure: Figure
  graphDiv: HTMLElement
  time: number
}

export const DEFAULT_MARGIN = { t: 0, r: 15, b: 0, l: 0 }
export const DEFAULT_HEIGHT = 450

export function dashCase(str: string): string {
  return str.toLowerCase().replace(/\W+/g, "-")
}

export default function PlotWrapper<TraceName extends string = string>(
  {
    params,
    src,
    alt,
    height,
    setXRange,
    basePath,
    className,
    download,
    onLegendClick,
    onLegendDoubleClick,
    onLegendMouseOver,
    onLegendMouseOut,
    onXRange,
    onRelayout,
  }: Props<TraceName>
) {
  const [ computedHeight, setComputedHeight ] = useState<number | null>(null)
  const legendRef = useRef<HTMLElement | null>(null)
  const [ initialized, setInitialized ] = useState<Initialized | null>(null)
  const [ firstRender ] = useState<Date>(new Date)

  // Log plot-initialization timing
  useEffect(
    () => {
      if (!initialized) return
      const { time } = initialized
      const delayMs = time - firstRender.getTime()
      console.log(`Plot "${alt}" initialized ${delayMs / 1000}s after initial render`)
    },
    [ initialized ]
  )

  // Optionally download plot
  const [ downloaded, setDownloaded ] = useState(false)
  useEffect(
    () => {
      if (!initialized) return
      if (downloaded) return
      if (!download) return
      if (download === true) download = 'png'
      if (typeof download === 'string') download = { format: download }
      const { figure, graphDiv } = initialized
      let {
        width = graphDiv.offsetWidth,
        height = graphDiv.offsetHeight,
        format,
        filename = `${dashCase(alt)}`,
      } = download
      format = format === 'jpg' ? 'jpeg' : (format ?? 'png')
      const downloadOpts: DownloadImgopts = { format, width, height, filename, }
      console.log("download plot:", downloadOpts)
      setDownloaded(true)
      import('plotly.js')
        .then(m => m.default)
        .then(Plotly => Plotly.downloadImage(figure, downloadOpts))
        .then(img => console.log('downloaded plot:', img))
    },
    [ initialized, download, downloaded ]
  )

  // Legend mouse{over,out} handlers
  useEffect(
    () => {
      // Add mouseover, mouseout handlers to ".groups" elems
      if (!initialized) return
      const { graphDiv } = initialized
      const legend = graphDiv.getElementsByClassName('legend')[0]
      if (!legend) return
      const legendGroups = Array.from(legend.getElementsByClassName('groups'))
      const handlers = {} as Record<'mouseover' | 'mouseout', (name: TraceName) => boolean>
      if (onLegendMouseOver) handlers.mouseover = onLegendMouseOver
      if (onLegendMouseOut) handlers.mouseout = onLegendMouseOut
      const listeners = legendGroups.map(
        (group, i) => [
          group,
          mapValues(
            handlers,
            (event, handler) => {
              const listener = () => {
                const { data } = params
                const { name } = data[i]
                if (name === undefined) {
                  console.error(`No data trace found at curve number ${i}:`, data)
                  return
                }
                handler(name as TraceName)
              }
              group.addEventListener(event, listener)
              return listener
            }
          )
        ] as const
      )
      return () => {
        listeners.forEach(
          ([ group, listeners ]) => {
            entries(listeners).forEach(([ event, handler ]) => group.removeEventListener(event, handler))
          }
        )
      }
    },
    [ initialized ]
  )

  const {
    data,
    layout,
    style
  } = params
  if (height === undefined) {
    if (computedHeight !== null) {
      height = computedHeight
    } else if (typeof style?.height === 'number') {
      height = style.height
    } else if (typeof layout?.height === 'number') {
      height = layout.height
    } else {
      height = DEFAULT_HEIGHT
    }
  }
  basePath = basePath || getBasePath() || ""
  src = `${basePath}/${src}`
  return (
    <div className={className ? `${css.plotWrapper} ${className}` : `${css.plotWrapper}`}>
      <div
        className={`${css.fallback} ${initialized ? css.hidden : ""}`}
        style={{height: `${height}px`, maxHeight: `${height}px`}}
      >{
        src && <>
            <Image
                src={src}
                alt={alt}
                fill
                loading="lazy"
            />
            <div className={css.spinner}></div>
          </>
      }</div>
      <Plotly
        onInitialized={(fig, div) => {
          const parent = div.offsetParent as HTMLElement
          const [legend] = div.getElementsByClassName('legend') as any as HTMLElement[]
          legendRef.current = legend
          setInitialized({ figure: fig, graphDiv: div, time: new Date().getTime() })
          setComputedHeight(parent.offsetHeight)
          if (onRelayout) {
            onRelayout({} as PlotRelayoutEvent, div as HTMLDivElement)
          }
        }}
        onDoubleClick={() => {
          if (setXRange) {
            setXRange(null)
          }
        }}
        onLegendClick={e => {
          if (onLegendClick) {
            const {curveNumber, data} = e
            const {name} = data[curveNumber]
            if (name === undefined) {
              console.error(`No data trace found at curve number ${curveNumber}:`, e)
              return true
            }
            return onLegendClick(name as TraceName)
          } else {
            return true
          }
        }}
        onLegendDoubleClick={e => {
          if (onLegendDoubleClick) {
            const {curveNumber, data} = e
            const {name} = data[curveNumber]
            if (name === undefined) {
              console.error(`No data trace found at curve number ${curveNumber}:`, e)
              return true
            }
            return onLegendDoubleClick(name as TraceName)
          } else {
            return true
          }
        }}
        onRelayout={e => {
          // console.log("relayout:", e, initialized?.graphDiv.offsetWidth, initialized?.graphDiv)
          if (onXRange && 'xaxis.range[0]' in e && 'xaxis.range[1]' in e) {
            let [ start, end ] = [ e['xaxis.range[0]'] as number, e['xaxis.range[1]'] as number, ]
            const newXRange = onXRange(start, end)
            if (setXRange && newXRange) {
              setXRange(newXRange)
            }
          }
          if (onRelayout) {
            onRelayout(e, initialized!.graphDiv as HTMLDivElement)
          }
        }}
        className={css.plotly}
        data={data}
        config={{ displayModeBar: false, scrollZoom: false, responsive: true, }}
        style={{ ...style, visibility: initialized ? undefined : "hidden", width: "100%", }}
        layout={layout}
        // onClick={() => setInitialized(false)}
      />
    </div>
  )
}
