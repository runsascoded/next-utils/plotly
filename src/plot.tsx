import React, { CSSProperties, ReactNode, useMemo, useState } from "react";
import css from "./plot.module.scss"
import { PlotParams } from "react-plotly.js";
import { Datum, Layout, Legend, Margin, PlotData } from "plotly.js";
import { fromEntries, o2a } from "@rdub/base/objs"
import PlotWrapper, { DEFAULT_HEIGHT, DEFAULT_MARGIN, LegendHandlers, OtherHandlers } from "./plot-wrapper";

export type PlotsDict<Params extends PlotParams = PlotParams> = { [id: string]: Params }

export type XRange = [number, number]
export type FilterArgs = {
  data: PlotData[]
  // [ xs, xe ]: [number, number]
  xRange: XRange
}
export type Filter = (_: FilterArgs) => PlotData[]

export const filterIdxs: Filter = ({ data, xRange, }: FilterArgs) => {
  const xs = Math.round(xRange[0])
  const xe = Math.round(xRange[1])
  return data.map(
    ({x, y, ...trace}) => ({
      x: x.slice(xs, xe),
      y: y.slice(xs, xe),
      ...trace,
    })
  )
}

export type FilterValuesArgs = {
  keepNull?: boolean
  mapRange?: (xRange: XRange) => XRange
}
export const filterValues: ({ keepNull, mapRange }: FilterValuesArgs) => Filter =
  ({ keepNull, mapRange }) =>
    ({ data, xRange }) => {
      keepNull = keepNull || keepNull === undefined
      const [ xs, xe ] = mapRange ? mapRange(xRange) : xRange
      // xs = Math.round(xs - 0.5) + 0.5
      // xe = Math.round(xe + 0.5) - 0.5
      return data.map(
        ({x, y, ...trace}) => {
          y = y as Datum[]
          const enumerated =
            (x as number[])
              .map((v, idx) => [ v, idx ])
              .filter(([ v ]) => (v === null ? keepNull : (xs <= v && v <= xe)))
          const idxs = enumerated.map(([ v, idx ]) => idx)
          return {
            x: enumerated.map(([ v ]) => v),
            y: y.filter((v, idx) => idxs.includes(idx)),
            ...trace,
          }
        }
      )
    }

export const HalfRoundWiden: (xRange: XRange) => XRange = ([ xs, xe ]) => {
  xs = Math.round(xs - 0.5) + 0.5
  xe = Math.round(xe + 0.5) - 0.5
  return [ xs, xe ]
}

export type PlotSpec = {
  id: string
  name: string
  menuName?: string
  dropdownSection?: string,
  title?: string  // taken from plot, by default
  subtitle?: ReactNode
  width?: number
  height?: number
  style?: CSSProperties
  legend?: "inherit" | Legend
  src?: string
  filter?: Filter
  children?: ReactNode
}

export type Plot<
  TraceName extends string = string,
  Params extends PlotParams = PlotParams
> = PlotSpec & {
  params: Params
  title: string
  heading?: ReactNode
  margin?: Partial<Margin>
  basePath?: string
} & LegendHandlers<TraceName> & OtherHandlers

export type Opts = { rmTitle?: boolean }
export const DefaultOpts: Opts = { rmTitle: true }

export function buildPlot<
  TraceName extends string = string,
  Params extends PlotParams = PlotParams
>(
  spec: PlotSpec,
  params: Params,
  opts: Opts = DefaultOpts,
): Plot<TraceName, Params> {
  const id = spec.id
  let title = spec.title
  if (!title) {
    let { layout: { title: plotTitle } } = params
    if (typeof plotTitle === 'string') {
      title = plotTitle
    } else if (plotTitle?.text) {
      title = plotTitle.text
    } else {
      console.error(`No title found for plot ${id}:`, params)
      throw new Error(`No title found for plot ${id}`)
    }
  }
  if (opts.rmTitle) {
    let { layout: { title: plotTitle, ...layout } } = params
    if (typeof plotTitle === 'string') {
      params = { ...params, layout }
    } else if (plotTitle) {
      const { text, ..._plotTitle } = plotTitle
      params = { ...params, layout: { ...layout, title: _plotTitle } }
    }
  }
  return { ...spec, title, params, }
}

export function buildPlots<
  TraceName extends string = string,
  Params extends PlotParams = PlotParams
>(
  specs: PlotSpec[],
  plots: { [id: string]: Params },
  opts: Opts = DefaultOpts,
): Plot<TraceName, Params>[] {
  const plotSpecDict: { [id: string]: PlotSpec } = fromEntries(specs.map(spec => [ spec.id, spec ]))
  return o2a(plots, (id, plot) => {
    const spec = plotSpecDict[id]
    if (!spec) return
    return buildPlot<TraceName, Params>(spec, plot, opts)
  }).filter((p): p is Plot<TraceName, Params> => !!p)
}

export function Plot<
  TraceName extends string = string,
  Params extends PlotParams = PlotParams
>(
  {
    id, name,
    title, subtitle,
    params, heading,
    height = DEFAULT_HEIGHT,
    src, margin,
    basePath,
    filter,
    children,
    ...rest
  }: Plot<TraceName, Params>
) {
  const {
    data: plotData,
    layout,
  } = params
  const [ xRange, setXRange ] = useState<null | [number, number]>(null)
  name = name || id
  if (src === undefined) {
    src = `plots/${name}.png`
  }
  const newLayout: Partial<Layout> = useMemo(
    () => {
      const { margin: plotMargin, xaxis, yaxis, ...rest } = layout
      return {
        margin: { ...DEFAULT_MARGIN, ...plotMargin, ...margin },
        dragmode: filter ? "zoom" : false,
        xaxis: {
          ...(filter ? {} : { fixedrange: true }),
          ...(xaxis || {}),
        },
        yaxis: {
          automargin: true,
          gridcolor: "#ccc",
          autorange: true,
          fixedrange: true,
          ...(yaxis || {}),
        },
        height,
        autosize: true,
        ...rest
      }
    },
    [ layout, margin, height, xRange, filter ]
  )

  const filteredTraces: PlotData[] = useMemo(() => {
    const data = plotData as PlotData[]
    return (filter && xRange) ? filter({ data, xRange }) : data
  }, [ plotData, xRange, filter ])

  return (
    <div id={id} key={id} className={css.plot}>
      {heading ?? <h2><a href={`#${id}`}>{title}</a></h2>}
      {subtitle ?? null}
      <PlotWrapper
        // id={id}
        params={{ ...params, data: filteredTraces, layout: newLayout }}
        src={src}
        alt={title}
        height={height}
        setXRange={setXRange}
        basePath={basePath}
        {...rest}
      />
      {children}
    </div>
  )
}
